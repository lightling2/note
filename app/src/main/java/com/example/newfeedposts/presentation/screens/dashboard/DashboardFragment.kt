package com.example.newfeedposts.presentation.screens.dashboard

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.newfeedposts.R
import com.example.newfeedposts.databinding.FragmentDashboardBinding
import com.example.newfeedposts.domain.models.Note
import com.example.newfeedposts.presentation.screens.viewmodels.DashboardViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */

@AndroidEntryPoint
class DashboardFragment : Fragment() {

    private var _binding: FragmentDashboardBinding? = null
    private val binding get() = _binding!!

    private val viewModel by viewModels<DashboardViewModel>()
    private val adapter by lazy { DashboardAdapter(::navigate) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentDashboardBinding.inflate(inflater, container, false)
        val recyclerView = binding.recyclerView
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        return binding.root
    }


    private fun navigate(note: Note){
        val action = DashboardFragmentDirections.actionDashboardFragmentToViewNoteFragment(note)
        findNavController().navigate(action)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()

        lifecycleScope.launch {
            viewModel.getPreferencesTitle().collectLatest {
                Log.d(TAG, "Current title: $it")
            }
        }
        lifecycleScope.launch {
            viewModel.setPreferencesTitle("old title")
        }

        binding.fab.setOnClickListener(){
            findNavController().navigate(R.id.action_DashboardFragment_to_AddNoteFragment)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initViews(){
        lifecycleScope.launch(Dispatchers.Main){
            viewModel.getNotes().collectLatest { list ->
                adapter.displayList(list)
                Log.d(TAG, "Current Note List: $list")
            }
        }
    }

    companion object {
        const val TAG = "DashboardTAG"
    }

}