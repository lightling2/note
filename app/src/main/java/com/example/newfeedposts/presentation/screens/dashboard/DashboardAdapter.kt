package com.example.newfeedposts.presentation.screens.dashboard

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.newfeedposts.databinding.NoteItemBinding
import com.example.newfeedposts.domain.models.Note

class DashboardAdapter(val navigateToDetails: (note: Note ) -> Unit ): RecyclerView.Adapter<DashboardAdapter.ListViewHolder>() {
    private var noteList = emptyList<Note>()
    class ListViewHolder(private val binding: NoteItemBinding): RecyclerView.ViewHolder(binding.root){
        fun attach(note: Note) = with(binding){
            tvTitle.text = note.title
            tvContent.text = note.body
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val binding = NoteItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ListViewHolder(binding).apply {
            binding.root.setOnClickListener{
                println("Item Clicked is: ${noteList[adapterPosition]}")
                navigateToDetails(noteList[adapterPosition])
            }
        }

    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        noteList[position].let { holder.attach(it) }
    }

    override fun getItemCount(): Int {
       return noteList.size
    }

    fun displayList(noteList: List<Note>){
        this.noteList = noteList
        notifyDataSetChanged()
    }

}