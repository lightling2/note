package com.example.newfeedposts.utils

import androidx.datastore.preferences.core.stringPreferencesKey

object PreferencesKeys {
    val TITLE = stringPreferencesKey("title")
    val BODY = stringPreferencesKey("body")
}