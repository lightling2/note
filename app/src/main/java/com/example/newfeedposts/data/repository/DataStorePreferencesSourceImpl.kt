package com.example.newfeedposts.data.repository

import android.content.Context
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.preferencesDataStore
import com.example.newfeedposts.domain.repository.DataStorePreferencesSource
import com.example.newfeedposts.utils.Constants
import com.example.newfeedposts.utils.PreferencesKeys
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map


private val Context.dataStore by preferencesDataStore(
    name = Constants.PREFERENCES
)

class DataStorePreferencesSourceImpl(private val context: Context): DataStorePreferencesSource {
    override fun getPreferencesTitle(): Flow<String> {
        return context.dataStore.data.map { preferences ->
            preferences[PreferencesKeys.TITLE] ?: ""
        }
    }

    override suspend fun setPreferencesTitle(title: String) {
        context.dataStore.edit { preferences ->
            preferences[PreferencesKeys.TITLE] = title
        }
    }

}