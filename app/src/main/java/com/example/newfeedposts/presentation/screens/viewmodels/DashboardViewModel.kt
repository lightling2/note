package com.example.newfeedposts.presentation.screens.viewmodels

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.newfeedposts.domain.models.Note
import com.example.newfeedposts.domain.repository.NoteRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DashboardViewModel @Inject constructor(private val noteRepo: NoteRepo): ViewModel() {

    private var _specificNote = MutableStateFlow(Note(title = "", body = ""))
    val specificNote = _specificNote.asStateFlow()

    fun getNotes(): Flow<List<Note>> {
        return noteRepo.getNotes()
    }

    fun getNoteById(id: Int) {
        viewModelScope.launch {
            val note = noteRepo.getNoteById(id)
            Log.d(TAG, "getNoteById: $note")
            _specificNote.value = note!!
        }
    }

    fun insertNote(note: Note) {
        viewModelScope.launch {
            Log.d(TAG, "current view model thread: ${Thread.currentThread().name}")
            noteRepo.insertNote(note)
        }

    }


    fun deleteNote(note: Note){
        viewModelScope.launch(Dispatchers.IO) {
            noteRepo.deleteNote(note)
        }
    }



    fun getPreferencesTitle(): Flow<String> {
        return noteRepo.getPreferencesTitle()
    }

    fun setPreferencesTitle(title: String) {
        viewModelScope.launch {
            noteRepo.setPreferencesTitle(title)
        }
    }

    companion object {
        const val TAG = "ViewModel"
    }

}