package com.example.newfeedposts.data.db

import androidx.room.*
import com.example.newfeedposts.domain.models.Note
import kotlinx.coroutines.flow.Flow

@Dao
interface NoteDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertNote(note: Note)

    @Delete
    fun deleteNote(note: Note)

    @Update
    fun updateNote(note: Note)

    @Query("select * from notes")
    fun getNotes(): Flow<List<Note>>

    @Query("select * from notes where id = :id")
    fun getNoteById(id: Int): Note?

}