package com.example.newfeedposts.presentation.screens.updatenote

import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.newfeedposts.R
import com.example.newfeedposts.databinding.FragmentUpdateNoteBinding
import com.example.newfeedposts.domain.models.Note
import com.example.newfeedposts.presentation.screens.viewmodels.DashboardViewModel
import com.example.newfeedposts.presentation.screens.viewnote.ViewNoteFragmentArgs
import com.example.newfeedposts.presentation.screens.viewnote.ViewNoteFragmentDirections

class UpdateNoteFragment : Fragment() {

    private var _binding: FragmentUpdateNoteBinding? = null
    private val binding get() = _binding!!

    private val args by navArgs<UpdateNoteFragmentArgs>()
    private val viewModel: UpdateNoteViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentUpdateNoteBinding.inflate(inflater, container, false)

        println("Here's the argument from updateNote: $args")
        return binding.root
        // Inflate the layout for this fragment
    }

    private fun initViews() = with(binding){
        etTitle.setText(args.currentNote.title)
        etContent.setText(args.currentNote.body)
        btnSaveNote.setOnClickListener {
            updateNote()
        }
    }

    private fun updateNote() = with(binding){
        val title = etTitle.text.toString()
        val content = etContent.text.toString()

        if (inputCheck(title, content)){
            val updatedNote = Note(args.currentNote.id, title, content)
            viewModel.updateNote(updatedNote)

            Toast.makeText(requireContext(), "Successfully Updated!", Toast.LENGTH_LONG).show()
            findNavController().navigate(R.id.action_updateNoteFragment_to_DashboardFragment)
        }else{
            Toast.makeText(requireContext(), "Please Fill Out all Fields.", Toast.LENGTH_LONG).show()
        }
    }

    private fun inputCheck(title: String, content: String): Boolean{
        return !(TextUtils.isEmpty(title) && TextUtils.isEmpty(content))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}