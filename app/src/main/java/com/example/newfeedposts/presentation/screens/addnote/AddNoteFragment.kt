package com.example.newfeedposts.presentation.screens.addnote

import android.os.Bundle
import android.text.TextUtils
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.newfeedposts.R
import com.example.newfeedposts.databinding.FragmentAddNoteBinding
import com.example.newfeedposts.domain.models.Note
import com.example.newfeedposts.presentation.screens.viewmodels.DashboardViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */

@AndroidEntryPoint
class AddNoteFragment : Fragment() {

    private var _binding: FragmentAddNoteBinding? = null
    private val binding get() = _binding!!

    private val viewModel by viewModels<DashboardViewModel>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentAddNoteBinding.inflate(inflater, container, false)
        setHasOptionsMenu(true)

        binding.btnAddNote.setOnClickListener(){
            insertNote()
        }
        return binding.root

    }

    private fun insertNote() = with(binding){
        val title = etTitle.text.toString()
        val content = etContent.text.toString()

        if(inputCheck(title, content)){
            val note = Note(title= title,body= content)
            viewModel.insertNote(note)

            Toast.makeText(requireContext(), "Note Successfully Added!", Toast.LENGTH_LONG).show()
            findNavController().navigate(R.id.action_AddNoteFragment_to_DashboardFragment)
        }else{
            Toast.makeText(requireContext(), "Please Fill Out All Fields.", Toast.LENGTH_LONG).show()
        }
    }

    private fun inputCheck(title: String, content: String): Boolean{
        return !(TextUtils.isEmpty(title) && TextUtils.isEmpty(content))
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_add_note, menu)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.add_note){
            println("Add Note Clicked")
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}