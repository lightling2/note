package com.example.newfeedposts.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.newfeedposts.domain.models.Note
import com.example.newfeedposts.utils.Constants.DATABASE_NAME

@Database(entities = [Note::class], version = 1)
abstract class NoteDatabase : RoomDatabase() {
    abstract fun getNoteDao(): NoteDao

}
