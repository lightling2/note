package com.example.newfeedposts.utils

object Constants {
    const val DATABASE_NAME = "notes_db"
    const val PREFERENCES = "datastore_preferences"

}