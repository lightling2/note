package com.example.newfeedposts.domain.repository

import kotlinx.coroutines.flow.Flow

interface DataStorePreferencesSource {

    fun getPreferencesTitle(): Flow<String>
    suspend fun setPreferencesTitle(title: String)

}