package com.example.newfeedposts.di

import android.content.Context
import androidx.room.Room
import com.example.newfeedposts.data.db.NoteDao
import com.example.newfeedposts.data.db.NoteDatabase
import com.example.newfeedposts.data.repository.DataStorePreferencesSourceImpl
import com.example.newfeedposts.data.repository.NoteRepoImpl
import com.example.newfeedposts.domain.repository.DataStorePreferencesSource
import com.example.newfeedposts.domain.repository.NoteRepo
import com.example.newfeedposts.utils.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
internal object AppModule {
    @Singleton
    @Provides
    fun provideRoom(@ApplicationContext context: Context): NoteDao = Room.databaseBuilder(
        context,
        NoteDatabase::class.java,
        Constants.DATABASE_NAME
    ).build().getNoteDao()

    @Singleton
    @Provides
    fun provideDataStore(@ApplicationContext context: Context): DataStorePreferencesSource {
        return DataStorePreferencesSourceImpl(context)
    }

    @Singleton
    @Provides
    fun provideRepo(noteDao: NoteDao, dataStore: DataStorePreferencesSource): NoteRepo {
        return NoteRepoImpl(noteDao, dataStore)
    }

}