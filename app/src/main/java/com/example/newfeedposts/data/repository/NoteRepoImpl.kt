package com.example.newfeedposts.data.repository

import android.content.Context
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.preferencesDataStore
import com.example.newfeedposts.data.db.NoteDao
import com.example.newfeedposts.data.db.NoteDatabase
import com.example.newfeedposts.domain.models.Note
import com.example.newfeedposts.domain.repository.DataStorePreferencesSource
import com.example.newfeedposts.domain.repository.NoteRepo
import com.example.newfeedposts.utils.Constants.PREFERENCES
import com.example.newfeedposts.utils.PreferencesKeys
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import javax.inject.Inject

class NoteRepoImpl @Inject constructor (
    private val noteDao: NoteDao,
    private val dataStore: DataStorePreferencesSource

) : NoteRepo {
    override fun getNotes(): Flow<List<Note>> {
        return noteDao.getNotes()
    }

    override suspend fun getNoteById(id: Int): Note? = withContext(Dispatchers.IO) {
        return@withContext noteDao.getNoteById(id)
    }

    override suspend fun insertNote(note: Note) = withContext(Dispatchers.IO) {
        noteDao.insertNote(note)
    }

    override suspend fun deleteNote(note: Note) = withContext(Dispatchers.IO){
        noteDao.deleteNote(note)
    }

    override suspend fun updateNote(note: Note) = withContext(Dispatchers.IO) {
        noteDao.updateNote(note)
    }

    override fun getPreferencesTitle(): Flow<String> {
        return dataStore.getPreferencesTitle()
    }

    override suspend fun setPreferencesTitle(title: String) {
       dataStore.setPreferencesTitle(title)
    }
}
