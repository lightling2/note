package com.example.newfeedposts.presentation.screens.updatenote

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.newfeedposts.domain.models.Note
import com.example.newfeedposts.domain.repository.NoteRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UpdateNoteViewModel @Inject constructor(private val noteRepo: NoteRepo): ViewModel() {

    fun updateNote(note: Note){
        viewModelScope.launch(Dispatchers.IO) {
            noteRepo.updateNote(note)
        }
    }
}