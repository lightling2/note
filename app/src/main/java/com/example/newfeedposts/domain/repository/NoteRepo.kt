package com.example.newfeedposts.domain.repository

import com.example.newfeedposts.domain.models.Note
import kotlinx.coroutines.flow.Flow

interface NoteRepo {

    fun getNotes(): Flow<List<Note>>

    suspend fun getNoteById(id: Int): Note?

    suspend fun insertNote(note: Note)

    suspend fun deleteNote(note: Note)

    suspend fun updateNote(note: Note)

    fun getPreferencesTitle(): Flow<String>

    suspend fun setPreferencesTitle(title: String)

}