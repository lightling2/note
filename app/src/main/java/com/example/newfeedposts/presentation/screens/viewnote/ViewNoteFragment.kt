package com.example.newfeedposts.presentation.screens.viewnote

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.newfeedposts.databinding.FragmentViewNoteBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ViewNoteFragment : Fragment() {

    private var _binding: FragmentViewNoteBinding? = null
    private val binding get() = _binding!!

    private val args by navArgs<ViewNoteFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentViewNoteBinding.inflate(inflater, container, false)

        println("Here's the argument from viewNote: $args")
        return binding.root

    }

    private fun initViews() = with(binding){
        tvTitle.text = args.currentNote.title
        tvContent.text = args.currentNote.body
        btnEditNote.setOnClickListener(){
            val action = ViewNoteFragmentDirections.actionViewNoteFragmentToUpdateNoteFragment(args.currentNote)
            findNavController().navigate(action)
        }
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}